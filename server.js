const webpack = require('webpack')
const WebpackDevServer = require('webpack-dev-server')
const config = require('./webpack.config')

new WebpackDevServer(webpack(config), {
  publicPath: config.output.publicPath,
  hot: true,
  historyApiFallback: true,
  quiet: false,
  stats: { colors: true },
  inline: true,
  compress: true,
  proxy: {
    '/api/**': {
      target: '',
      changeOrigin: true,
      secure: false
    }
  }
}).listen(8090, '0.0.0.0', function (error, result) {
  if (error) {
    return console.log(error)
  }
  console.log(`Listening at http://0.0.0.0:8090/`)
})
