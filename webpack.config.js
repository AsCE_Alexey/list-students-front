const path = require('path')
const webpack = require('webpack')

module.exports = {
  devtool: 'source-map',
  entry: [ './src/index' ],
  mode: 'production',
  output: {
    path: path.resolve(__dirname, 'build'),
    publicPath: '/build/',
    filename: 'bundle.js'
  },
  resolve: {
    extensions: [ '.js', '.jsx', '.svg', '.mp3' ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    })
    // new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en|ru/)
  ],
  module: {
    rules: [ {
      test: /\.jsx?$/,
      exclude: /(node_modules|bower_components)/,
      use: 'babel-loader'
    }, {
      test: /\.scss$/,
      use: [ 'style-loader', 'css-loader', 'sass-loader' ]
    }, {
      test: /\.css$/,
      use: [ 'style-loader', 'css-loader' ]
    }, {
      test: /\.svg$/,
      include: [
        path.resolve(__dirname, 'src/icons/')
      ],
      use: [ 'babel-loader', 'svg-react-loader' ]
    }, {
      test: /\.svg$/,
      exclude: [
        path.resolve(__dirname, 'src/icons/')
      ],
      use: 'url-loader?name=images/img-[sha512:Hash:base64:9].[ext]&limit=8192'
    }, {
      test: /\.sass$/,
      use: [ 'style-loader', 'css-loader', 'sass-loader' ]
    }, {
      test: /\.(png|jpg)(\?v.*)?$/,
      use: 'file-loader?name=images/icons/[name].[ext]'
    }, {
      test: /\.(eot|woff2?|ttf|svg)(\?v.*)?$/,
      exclude: [
        path.resolve(__dirname, 'src/icons/')
      ],
      use: 'file-loader?name=fonts/[name].[ext]'
    } ]
  }
}
