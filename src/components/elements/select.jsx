import React from 'react'
import ReactDOM from 'react-dom'

export default class Select extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      isModal: false,
      position: null
    }
    this.isCursor = true
    this.domNode = document.createElement('div')
    this.renderModal = this.renderModal.bind(this)
    this.handleUpdate = this.handleUpdate.bind(this)
  }

  componentDidMount () {
    this.handleUpdate()
    document.body.appendChild(this.domNode)
  }
  componentWillUnmount () {
    document.body.removeChild(this.domNode)
  }
  handleUpdate () {
    this.setState({
      isModal: false,
      position: this.select && this.select.getBoundingClientRect()
    })
  }

  render () {
    const {
      defaultName,
      optionId = ''
    } = this.props
    return ([
      <div
        ref={node => {
          if (node && !this.select) {
            this.forceUpdate()
          }
          this.select = node || this.select
        }}
        onMouseEnter={() => { this.isCursor = false }}
        onMouseLeave={() => { this.isCursor = true }}
        className='select'>
        <div className='select__wrapper'>
          <button
            title={optionId || ''}
            onClick={() => {
              this.handleUpdate()
              this.setState({
                isModal: !this.state.isModal
              })
            }}
            className='select__value'>
            {optionId || defaultName}
          </button>
        </div>
      </div>,
      this.state.isModal && ReactDOM.createPortal(
        this.renderModal(),
        this.domNode
      )
    ])
  }

  renderModal () {
    const { onClick, options, optionId } = this.props
    return (
      <div
        style={{
          width: this.select && this.select.offsetWidth,
          top: this.state.position && this.state.position.top + 45,
          left: this.state.position.x
        }}
        tabIndex='0'
        onBlur={() => this.isCursor && this.setState({ isModal: false, isInput: false, search: '' })}
        onMouseLeave={() => { this.isCursor = true }}
        onMouseEnter={() => { this.isCursor = false }}
        className='select__modal'>
        {optionId && <button
          className='select__option'
          onClick={() => {
            typeof onClick === 'function' && onClick('')
            this.setState({ isModal: false })
          }}>
          clear
        </button>}
        {
          Object.keys(options).map(optionKey =>
            <button
              id={optionKey}
              key={optionKey}
              className='select__option'
              onClick={() => {
                typeof onClick === 'function' && onClick(optionKey)
                this.setState({ isModal: false })
              }}>
              {options[optionKey]}
            </button>
          )
        }
      </div>
    )
  }
}
