import React from 'react'
import classNames from 'classnames'

export default class Input extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      isFocus: false
    }
  }
  render () {
    const {
      className,
      disabled,
      label,
      onChange,
      placeholder,
      value,
      min,
      max,
      type
    } = this.props
    const { isFocus } = this.state
    return (
      <div className='input'>
        <input
          title={value}
          ref='input'
          type={type}
          value={value}
          min={min}
          max={max}
          onFocus={() => this.setState({ isFocus: true })}
          onBlur={() => this.setState({ isFocus: false })}
          onChange={evt => typeof onChange === 'function' && onChange(evt)}
          placeholder={(label && isFocus && placeholder) || (!label && placeholder)}
          className={classNames('input__field', className, {
            'input__field__nolabel': !label
          })}
        />
        {label && <div className={classNames('input__label__wrapper', {
          'input__label__wrapper__focus': isFocus || value
        })}>
          <div
            onClick={() => {
              !disabled && this.setState({ isTime: true })
              this.refs.input.focus()
            }}
            className={classNames('input__label', {
              'input__label__focus': isFocus || value,
              'input__label__focus__color': isFocus
            })}>
            {label}
          </div>
        </div>}
      </div>
    )
  }
}
