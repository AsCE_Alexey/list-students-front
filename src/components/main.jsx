import React from 'react'
import { connect } from 'react-redux'
import { actions } from '../reducer/students'
import Input from './elements/input'
import Select from './elements/select'
import CrossIcon from '../icons/cross.svg'

function stateToProps (state) {
  return {
    students: state.students
  }
}

const dispatchToProps = {
  create: actions.create,
  update: actions.update,
  remove: actions.remove,
  reset: actions.reset
}

@connect(stateToProps, dispatchToProps)
export default class Main extends React.Component {
  render () {
    const {
      students,
      create,
      update,
      remove,
      reset
    } = this.props
    const progressOptions = {
      unsatisfactory: ' Неудовлетворительно',
      satisfactorily: 'Удовлетворительно',
      good: 'Хорошо',
      perfect: 'Отлично'
    }
    const max = getDate(Date.now())
    return (
      <div className='students'>
        <div className='students__header'>
          <button
            className='students__button'
            onClick={() => create()}>
            Добавить студента
          </button>
          <button
            className='students__button__link'
            onClick={() => reset()}>
            Сбросить все
          </button>
        </div>
        <div className='students__content'>
          {Object.keys(students).map(studentId => <div className='students__line'>
            <div className='students__block'>
              <Input
                value={students[studentId].surname || ''}
                placeholder='Фамилия'
                label='Фамилия'
                onChange={evt => update({ id: studentId, key: 'surname', value: (evt.target.value || '') })}
              />
            </div>
            <div className='students__block'>
              <Input
                value={students[studentId].name || ''}
                placeholder='Имя'
                label='Имя'
                onChange={evt => update({ id: studentId, key: 'name', value: (evt.target.value || '') })}
              />
            </div>
            <div className='students__block'>
              <Input
                value={students[studentId].patronymic || ''}
                placeholder='Отчество'
                label='Отчество'
                onChange={evt => update({ id: studentId, key: 'patronymic', value: (evt.target.value || '') })}
              />
            </div>
            <div className='students__block'>
              <Input
                value={(students[studentId].born > max && max) || students[studentId].born || ''}
                max={max}
                type='date'
                onChange={evt => update({ id: studentId, key: 'born', value: (evt.target.value || '') })}
              />
            </div>
            <div className='students__block'>
              <Select
                defaultName='Успеваемость'
                optionId={students[studentId].progress}
                options={progressOptions}
                onClick={evt => update({ id: studentId, key: 'progress', value: progressOptions[evt] })}
              />
            </div>
            <button
              className='students__remove'
              onClick={() => remove(studentId)}>
              <CrossIcon />
            </button>
          </div>)}
        </div>
      </div>
    )
  }
}


function getDate (timestamp) {
  var currentDate = new Date(timestamp)
  var date = ('0' + currentDate.getDate()).slice(-2)
  var month = ('0' + (currentDate.getMonth() + 1)).slice(-2)
  var year = currentDate.getFullYear()
  return `${year}-${month}-${date}`
}
