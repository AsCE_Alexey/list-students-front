import './style/index.sass'
import Main from './components/main'
import { Provider } from 'react-redux'
import React from 'react'
import ReactDOM from 'react-dom'
import reducer from './reducer'
import { compose, createStore } from 'redux'

export const store = compose()(createStore)(reducer)

const parentElement = document.createElement('div')
document.body.appendChild(parentElement)

const originalConsoleError = console.error
if (console.error === originalConsoleError) {
  console.error = (...args) => {
    if (args[0] && args[0].indexOf && args[0].indexOf('[React Intl] Missing message:') === 0) {
      return
    }
    originalConsoleError.call(console, ...args)
  }
}
window.stateStore = store

ReactDOM.render(
  <Provider store={store}>
    <Main />
  </Provider>, parentElement)
