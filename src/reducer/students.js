import assocPath from 'ramda/src/assocPath'
import dissocPath from 'ramda/src/dissocPath'
import shortId from 'shortid'
import students from '../students.json'

export const types = {
  CREATE: 'students/create',
  REMOVE: 'students/remove',
  UPDATE: 'students/update',
  RESET: 'students/reset'
}

const defaultState = {...students}

const initialState = window.localStorage.students ? JSON.parse(window.localStorage.students) : defaultState

export default function reducer (state = initialState, action) {
  switch (action.type) {
    case types.CREATE:
      const id = shortId()
      const newUser = {
        surname: null,
        name: null,
        patronymic: null,
        born: null,
        progress: null
      }
      const resultCreateUser = assocPath([id], newUser, state)
      window.localStorage.students = JSON.stringify(resultCreateUser)
      return resultCreateUser

    case types.UPDATE:
      const resultUsers = assocPath([action.payload.id, action.payload.key], action.payload.value, state)
      window.localStorage.students = JSON.stringify(resultUsers)
      return resultUsers

    case types.REMOVE:
      console.log(action.payload)
      const resultRemoveUser = dissocPath([action.payload], state)
      window.localStorage.students = JSON.stringify(resultRemoveUser)
      return resultRemoveUser

    case types.RESET:
      window.localStorage.removeItem('students')
      return defaultState
    default:
      return state
  }
}

export const actions = {
  reset: () => ({ type: types.RESET }),
  update: payload => ({ type: types.UPDATE, payload }),
  create: payload => ({ type: types.CREATE, payload }),
  remove: payload => ({ type: types.REMOVE, payload })
}
